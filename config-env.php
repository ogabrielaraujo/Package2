<?php

	# connection
	define('DB_NAME', 				'$db');
	define('DB_USER', 				'$user');
	define('DB_PASSWORD', 			'$pass');
	define('DB_HOST', 				'$host');

	# debug mode
	define('WP_DEBUG', 				$debug);
	define('WP_DEBUG_DISPLAY', 		$debug);
	define('SCRIPT_DEBUG', 			$debug);
	define('WP_DEBUG_LOG', 			$debug);

	# disallow file edit
	define('DISALLOW_FILE_EDIT', 	$edit);
	define('DISALLOW_FILE_MODS', 	$edit);