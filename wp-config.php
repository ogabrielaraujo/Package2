<?php

# Environments
$env = array(
	'local' 	=> array('.local', '.local.com', '.local.com.br', 'localhost', '.dev'),
	'beta' 		=> '.esy.es',
	'prod' 		=> array('.com', '.com.br'),
);

# define WP_ENV and include config-env.php
foreach ( $env AS $env_name => $env_host ) {

	if ( is_array($env_host) ) {
		foreach ($env_host as $option) {
			if ( stristr($_SERVER['HTTP_HOST'], $option) ) {
				define('WP_ENV', $env_name);
			}
		}
	} else {
		if ( stristr($_SERVER['HTTP_HOST'], $env_host) ) {
			define( 'WP_ENV', $env_name );
		}
	}

	if ( $env_name == WP_ENV ) {
		include_once 'config-' . WP_ENV . '.php';
	}

}

# If no environment is set default to production
if ( !defined('WP_ENV') ) {
	define( 'WP_ENV', 'prod' );
}

// post revision
define('WP_POST_REVISIONS', 10);

// Database configs
if (!defined('DB_CHARSET')) { define('DB_CHARSET', 'utf8'); }
if (!defined('DB_COLLATE')) { define('DB_COLLATE', ''); }

// Keys : https://api.wordpress.org/secret-key/1.1/salt
$keys

// Table prefix
$table_prefix  = 'app_';

// Concatenate scripts on wp-admin (fix bugs)
define('CONCATENATE_SCRIPTS', false);

// Language
define('WPLANG', 'pt_BR');

// Absolute path
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

// Set custom paths
define('WP_SITEURL',		'http://' . $_SERVER['SERVER_NAME'] . '/app');
define('WP_HOME',			'http://' . $_SERVER['SERVER_NAME'] . '');
define('WP_CONTENT_URL',	'http://' . $_SERVER['SERVER_NAME'] . '/content');
define('WP_CONTENT_DIR',	dirname(__FILE__) . '/content');
define('WP_CORE_DIR',		dirname(__FILE__) . '/app');

// load settings
require_once(ABSPATH . 'wp-settings.php');