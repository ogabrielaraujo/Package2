<?php

	if ( file_exists('./app/wp-blog-header.php') ) {

		# Tells WordPress to load the WordPress theme and output it.
		define('WP_USE_THEMES', true);

		# Loads the WordPress Environment and Template
		require('./app/wp-blog-header.php');

	} else {

		echo 'Silence is golden :)';

	}